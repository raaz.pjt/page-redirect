<?php
/*
Plugin Name:  raj
Description:  Example plugin for the video tutorial series, "WordPress: Plugin Development", available at Lynda.com.
Plugin URI:   https://profiles.wordpress.org/specialk
Author:       Raj Prajapati
Version:      1.0
Text Domain:  raj
Domain Path:  /languages
License:      GPL v2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.txt
*/

// exit if file is called directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
function raj_register_settings() {

    /*

    register_setting(
        string   $option_group,
        string   $option_name,
        callable $sanitize_callback
    );

    */

    register_setting(
        'raj_options', // name of the option group.
        'raj_options', // name of the option itself.
        'raj_callback_validate_options' // callback function to create validation.
    );
    /*

add_settings_section(
    string   $id,
    string   $title,
    callable $callback,
    string   $page
);

*/

    add_settings_section(
        'raj_section_login', // login page option -- section ID
        'Customize Login Page', // admin area option -- title of the section
        'raj_callback_section_login', // callback function -- display the description
        'raj' // page on which section should be displayed.
    );

    add_settings_section(
        'raj_section_admin',
        'Customize Admin Area',
        'raj_callback_section_admin',
        'raj'
    );

    /*

    add_settings_field(
        string   $id,
        string   $title,
        callable $callback,
        string   $page,
        string   $section = 'default',
        array    $args = []
    );

    */

    add_settings_field(
        'custom_url',
        'Custom URL',
        'raj_callback_field_text',
        'raj',
        'raj_section_login',
        [ 'id' => 'custom_url', 'label' => 'Custom URL for the login logo link' ]
    );

    add_settings_field(
        'custom_title',
        'Custom Title',
        'raj_callback_field_text',
        'raj',
        'raj_section_login',
        [ 'id' => 'custom_title', 'label' => 'Custom title attribute for the logo link' ]
    );

    add_settings_field(
        'custom_style',
        'Custom Style',
        'raj_callback_field_radio',
        'raj',
        'raj_section_login',
        [ 'id' => 'custom_style', 'label' => 'Custom CSS for the Login screen' ]
    );

    add_settings_field(
        'custom_message',
        'Custom Message',
        'raj_callback_field_textarea',
        'raj',
        'raj_section_login',
        [ 'id' => 'custom_message', 'label' => 'Custom text and/or markup' ]
    );

    add_settings_field(
        'custom_footer',
        'Custom Footer',
        'raj_callback_field_text',
        'raj',
        'raj_section_admin',
        [ 'id' => 'custom_footer', 'label' => 'Custom footer text' ]
    );

    add_settings_field(
        'custom_toolbar',
        'Custom Toolbar',
        'raj_callback_field_checkbox',
        'raj',
        'raj_section_admin',
        [ 'id' => 'custom_toolbar', 'label' => 'Remove new post and comment links from the Toolbar' ]
    );

    add_settings_field(
        'custom_scheme',
        'Custom Scheme',
        'raj_callback_field_select',
        'raj',
        'raj_section_admin',
        [ 'id' => 'custom_scheme', 'label' => 'Default color scheme for new users' ]
    );




}
add_action( 'admin_init', 'raj_register_settings' );