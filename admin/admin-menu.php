<?php
/*
Plugin Name:  raj
Description:  Example plugin for the video tutorial series, "WordPress: Plugin Development", available at Lynda.com.
Plugin URI:   https://profiles.wordpress.org/specialk
Author:       Raj Prajapati
Version:      1.0
Text Domain:  myplugin
Domain Path:  /languages
License:      GPL v2 or later
License URI:  https://www.gnu.org/licenses/gpl-2.0.txt
*/

// exit if file is called directly
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}


// add top-level administrative menu
function raj_add_toplevel_menu() {

    /*
        add_menu_page(
            string   $page_title,
            string   $menu_title,
            string   $capability,
            string   $menu_slug,
            callable $function = '',
            string   $icon_url = '',
            int      $position = null
        )
    */

    add_menu_page(
        'raj Settings',
        'raj',
        'manage_options',
        'raj',
        'raj_display_settings_page',
        'dashicons-admin-generic',
        null
    );

}
add_action( 'admin_menu', 'raj_add_toplevel_menu' );